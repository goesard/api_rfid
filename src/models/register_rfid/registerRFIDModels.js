const reg_RFID_Header =
  "insert into register.apps_register_rfid_headers (header_id, item_code, item_description, item_id, item_type, item_category, item_merk, item_manufacture, lot_number, quantity, register_note, creation_date) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)";

const reg_RFID_Lines =
  "insert into register.apps_register_rfid_lines (header_id, line_number, serial_number, rfid_number, rfid, note ,creation_date) values ($1,$2,$3,$4,$5,$6,$7)";

const qtyDue =
  "select sum(quantity) from register.apps_register_rfid_headers where lot_number = $1";

module.exports = { reg_RFID_Header, reg_RFID_Lines, qtyDue };
