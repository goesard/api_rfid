const regHeaderIDSeqn =
  "select seqn_value from public.apps_mst_seqn where seqn_id = 1";

const updateHeaderIDSeqn =
  "update public.apps_mst_seqn set seqn_value = seqn_value+1 where seqn_id = 1";

module.exports = { regHeaderIDSeqn, updateHeaderIDSeqn };
