const {Router} = require('express')
const router = Router()
const controller = require('../../controllers/utilities/seqnControllers')

router.get('/regheaderidseqn', controller.regHeaderIDSeqn)

module.exports = router 