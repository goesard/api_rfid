// DEPEDENCIES IMPORT
const Router = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

// MODULE IMPORT
const oraControllers = require("../../controllers/utilities/oraControllers");

const router = Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(cors());

// ROUTING FOR ITEM CODE
router.get("/itemCode", oraControllers.itemCodeLOV);

// ROUTING FOR LOT NUMBER
router.get("/lotnumber", oraControllers.lotNumber);

// ROUTING FOR TRANSACTION_SITEBRANCH
router.get("/transactionSiteBranch", oraControllers.transaction_sitebranch)

module.exports = router;
