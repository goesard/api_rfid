const {Router} = require('express')
const router = Router()
const controller = require('../../controllers/register_rfid/registerRFIDControllers')

router.post('/RFIDLine', controller.register_RFID)

module.exports = router 