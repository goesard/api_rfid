const oracledb = require("oracledb");

const dbConfig = {
  user: "apps",
  password: "Ebsapps53",
  connectString:
    // ENV DEV
    //"(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.42.131)(PORT = 1531))(CONNECT_DATA =(SID= BTSDEV)))",
    // ENV TEST
    // "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.42.165)(PORT = 1551))(CONNECT_DATA =(SID= BTSTEST)))",
    // ENV TEST
    "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.42.161)(PORT = 1521))(CONNECT_DATA =(SID= BTSPROD)))",
  externalAuth: false, // Set to true if using External Authentication
  poolMin: 10,
  poolMax: 50,
  poolIncrement: 10,
  poolPingInterval: 60,
  poolTimeout: 1,
  queueTimeout: 0,
  poolAlias: "APPS",
};

async function initOracleClient() {
  try {
    await oracledb.initOracleClient({ libDir: "D:/instantclient_21_12" });
    console.log("Oracle client initialized successfully");
  } catch (error) {
    console.error("Error initializing Oracle client:", error.message);
    process.exit(1);
  }
}

// Initialize Oracle client
async function initpool() {
  try {
    await oracledb.createPool(dbConfig);
    console.log("Oracle pool created successfully");
  } catch (error) {
    console.error("Error creating Oracle pool:", error.message);
    process.exit(1);
  }
}

async function oraConnection() {
  try {
    initOracleClient().then(() => {
      initpool().then(() => {
        console.log("Connected to Oracle");
      });
    });
  } catch (e) {
    console.error(`Error ${e}`);
  }
}

module.exports = oraConnection;
