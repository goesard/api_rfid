const { Pool } = require("pg");

const pool = new Pool({
  user: "dev",
  host: "34.101.212.76",
  database: "dev",
  password: "dev123",
  port: 5511, // default PostgreSQL port
});

async function pgConnections() {
  try {
    const testConnect = pool.connect((err, client, done) => {
      if (err) {
        console.error("Error connecting to postgres", err);
      } else {
        console.log("Connected to postgres");
        done(); // Release the client back to the pool
      }
      return testConnect;
    });
  } catch (e) {
    console.log(e);
  }
}

module.exports = { pgConnections, pool};
