const {pool} = require('../../connection/pgConnection')
const queries = require('../../models/utilities/seqnModels')

const regHeaderIDSeqn = (req, res) => {
    pool.query(queries.regHeaderIDSeqn, (error, results) => {
        if (error) throw error
        else
        res.status(200).json(results.rows)
    return results.rows
    })
}

module.exports = {
    regHeaderIDSeqn
}