const oracledb = require("oracledb");
const queries = require("../../models/utilities/oraModels");

const itemCodeLOV = async (req, res) => {
  try {
    const connection = await oracledb.getConnection("APPS");
    const result = await connection.execute(queries.itemCode, [], {
      outFormat: oracledb.OUT_FORMAT_OBJECT,
    });
    return res.status(200).json(result.rows);
  } catch (e) {
    res.status(500).json(e);
    console.error(e);
  }
};

const lotNumber = async (req, res) => {
  try {
    const connection = await oracledb.getConnection("APPS");
    const result = await connection.execute(queries.lotNumber, [], {
      outFormat: oracledb.OUT_FORMAT_OBJECT,
    });
    return res.status(200).json(result.rows);
  } catch (e) {
    console.log(e);
  }
};

const transaction_sitebranch = async (req, res) => {
  try {
    const connection = await oracledb.getConnection("APPS");
    const result = await connection.execute(
      queries.transaction_sitebranch,
      [],
      {
        outFormat: oracledb.OUT_FORMAT_OBJECT,
      }
    );
    return res.status(200).json(result.rows);
  } catch (e) {
    console.log(e);
    return res.status(500).json(e);
  }
};

module.exports = { itemCodeLOV, lotNumber, transaction_sitebranch };
