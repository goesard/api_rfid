const { format, max } = require("date-fns");
const { pool } = require("../../connection/pgConnection");
const queries = require("../../models/register_rfid/registerRFIDModels");
const Seqn = require("../../models/utilities/seqnModels");
const oraQueries = require("../../models/utilities/oraModels");
const oracledb = require("oracledb");

const register_RFID = async (req, res) => {
  const datas = req.body;
  // GENERATE SEQUENCE HEADER ID
  const headerID = await headeridSeqn();
  const dataLength = datas.lines.length;
  console.log(datas.header.p_lotNumber);
  const getQty = await getQTY(datas.header.p_lotNumber);
  const dueQty = getQty.ORAPOQTY - getQty.REGRFID;
  console.log(getQty);
  if (dataLength <= dueQty) {
    const line = await rfid_line(datas, headerID);
    // CHECK INSERT LINE SUCCESS OR NOT
    if (line.success == false) {
      console.log(line.message);
      return res.status(500).json({
        message: `Register Failed Lines : ${line.message}`,
        success: false,
      });
    }
    // RUNNING HEADER IF LINE SUCCESS
    const header = await rfid_header(datas, headerID);
    if (header.success == false) {
      console.log(header);
      return res.status(500).json({
        message: `Register Failed header : ${line.message}`,
        success: false,
      });
    }
    // UPDATE SEQUENCE HEADER ID
    updateHeaderID();
  } else {
    return res.status(500).json({
      message: `Register Failed : ${dueQty} Line: ${dataLength}`,
      success: false,
    });
  }
  return res.status(500).json({
    message: "Register Complete",
    success: true,
  });
};

const register_RFID1 = async (req, res) => {
  const datas = req.body;
  const line = await rfid_line(datas, 1);
  console.log(line);
  return res.status(200).json({
    message: `Register Failed Lines : ${line}`,
    success: true,
  });
};

async function getQTY(p_po_number) {
  return new Promise(async (resolve, reject) => {
    const connection = await oracledb.getConnection("APPS");
    const result = await connection.execute(oraQueries.qtyDue(p_po_number), [], {
      outFormat: oracledb.OUT_FORMAT_OBJECT,
    });
    const poQTYRFID = await pool.query(queries.qtyDue, [p_po_number]);

    if (poQTYRFID.rows[0].sum == null) {
      poQTYRFID.rows[0].sum = 0;
    }

    // console.log(poQTYRFID.rows[0].sum);

    const retval = {
      ORAPOQTY: result.rows[0].QTY,
      REGRFID: poQTYRFID.rows[0].sum,
    };
    resolve(retval);
  });
}

async function headeridSeqn() {
  return new Promise((resolve, reject) => {
    const seqn = pool.query(Seqn.regHeaderIDSeqn, (error, results) => {
      if (error) {
        resolve(error);
      }
      resolve(results.rows[0].seqn_value);
    });
  });
}

async function updateHeaderID() {
  return new Promise((resolve, reject) => {
    pool.query(Seqn.updateHeaderIDSeqn, (error, results) => {
      if (error) {
        resolve(error);
      }
      resolve(results);
    });
  });
}

async function rfid_header(datas, headerID) {
  return new Promise((resolve, reject) => {
    const results = datas.header;
    const currentDate = new Date();
    const formattedDate = format(currentDate, "RRRR-MM-dd");

    pool.query(
      queries.reg_RFID_Header,
      [
        headerID, // Header Id
        datas.header.p_itemCode, // Item Code
        datas.header.p_itemDesc, // Item Description
        datas.header.p_itemID, // Item ID
        datas.header.p_itemType, // Item Type
        datas.header.p_itemCategories, // Item Category
        datas.header.p_itemMerk, // Item Merk
        datas.header.p_itemManufacture, // Item Manufacture
        datas.header.p_lotNumber, // Lot Number or PO Number
        datas.header.p_quantity, // PO Quantity
        datas.header.p_registerNote, // Register Note
        formattedDate, //Creation Date
      ],
      (error, results) => {
        if (error) {
          const retval = {
            message: error.message,
            success: false,
          };
          resolve(retval);
        }
        const retval = {
          message: "Success Insert Header",
          success: true,
        };
        resolve(retval);
      }
    );
  });
}

async function rfid_line(datas, headerID) {
  return new Promise(async (resolve, reject) => {
    const currentDate = new Date();
    const formattedDate = format(currentDate, "RRRR-MM-dd");
    const dataArray = datas.lines;
    dataArray.forEach((data, index) => {
      //
      const retval = {
        message: "1",
        success: true,
      };
      resolve(retval);

      pool.query(
        queries.reg_RFID_Lines,
        [
          headerID,
          index + 1,
          data.serial_number,
          data.rfid_number,
          data.rfid,
          data.rfid_note,
          formattedDate,
        ],
        (error, results) => {
          if (error) {
            const retval = {
              message: error.message,
              success: false,
            };
            reject(retval);
          } else {
            const retval = {
              message: "Success Insert Line",
              success: true,
            };
            resolve(retval);
          }
        }
      );
    });
  });
}

module.exports = {
  register_RFID,
};
