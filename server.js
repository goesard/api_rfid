const bodyParser = require('body-parser');
const {pgConnections} = require("./src/connection/pgConnection");
const oraConnections = require("./src/connection/oraConnection");

const oraRouter = require("./src/routes/utilities/oraRoutes")
const seqn = require("./src/routes/utilities/seqnRoutes")

const registerRFID = require("./src/routes/register_rfid/registerRFIDRoutes")

const express = require("express");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req,res) => {
  res.status(200).json('test')
})

app.use("/api/ora/v1", oraRouter);
app.use("/api/seqn/v1", seqn);
app.use("/api/v1/register", registerRFID);

oraConnections().then(() => {
  pgConnections().then(() => {
    const PORT = process.env.PORT || 3000;
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  });
});
